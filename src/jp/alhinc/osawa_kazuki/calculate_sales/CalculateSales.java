package jp.alhinc.osawa_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		// System.out.println("ここにあるファイルを開きます => " + args[0]);
		//
		if (args.length !=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// コマンドライン引数で渡すフォルダ = C:\Users\osawa.kazuki\Desktop\売り上げ集計課題

		BufferedReader br = null;

		// Mapを2種類store（支店コード、支店名）sale（支店コード、金額）作成 箱を作る
		Map<String, String> store = new HashMap<String, String>();
		Map<String, Long> sale = new HashMap<String, Long>();

		// args[]にあるbranch.lst
		try {
			File file = new File(args[0], "branch.lst");
			if (!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// readLineで一行ずつ読む
			String line;
			while((line = br.readLine()) != null) {

				String[] branch = line.split(",",0);

				if(!(branch[0].matches("^[0-9]{3}"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
		        }

				if(branch.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				store.put(branch[0], branch[1]);
				sale.put(branch[0], 0L);
			}

				// {001=札幌支店, 002=仙台支店, 003=東京支店, 004=名古屋支店, 005=大阪支店}
				// System.out.println(store);

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br !=null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		List<File> branchOfSales = new ArrayList<File>();

		// Fileクラスのオブジェクトを生成する、args[0]で全部
		File dir = new File(args[0]);

		// listFilesメソッドを使用して一覧を取得する
		File[] list = dir.listFiles();

		for (int i = 0; i < list.length; i++) {
			// 正規表現参照 list.length = 7
			if(list[i].getName().matches("^[0-9]{8}.rcd$") && (list[i].isFile())) {
				branchOfSales.add(list[i]);
			}
		}

		// 2つずつで計算、1つ分少なくするsize()-1
		for (int i = 0; i < branchOfSales.size()-1; i++) {
		    File code = branchOfSales.get(i);
		    String subcode = code.getName();
		    String sub = subcode.substring(0,8);
			int codeNum1 = Integer.parseInt(sub);

			// get().getName()もできる?

			File code1 = branchOfSales.get(i+1);
		    String subcode1 = code1.getName();
		    String sub1 = subcode1.substring(0,8);
			int codeNum2 = Integer.parseInt(sub1);

			// returnを入れてループ終了
			if (codeNum2 - codeNum1 != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// branchOfSales：8桁+rcdが横並び
		// System.out.println(branchOfSales);

		// 格納されている要素数を確認 ループをまわして１行ずつ読み込む
		for (int i = 0; i < branchOfSales.size(); i++) {
			try {
			    List<String> codeSales = new ArrayList<String>();


			    FileReader fr = new FileReader(branchOfSales.get(i));
			    br = new BufferedReader(fr);

			    String line;
			    while((line = br.readLine()) != null) {
				    codeSales.add(line);
			    }

			    if(!(codeSales.size() == 2)) {
			    	System.out.println(branchOfSales.get(i).getName() + "のフォーマットが不正です");
			    	return;
			    }

			    if(!(store.containsKey(codeSales.get(0)))) {
				    System.out.println(branchOfSales.get(i).getName() + "の支店コードが不正です");
				    return;
				 }

			    if (!(codeSales.get(1).matches("^[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
					}

                // 支店コード[0]、売り上げ金額[1]
			    // System.out.println(codeSales);

				// 直前でやってたcodeSales、下記で 0 + get(1)が実現
				// 加算する string型をLong型に parse○○
				long price = Long.parseLong(codeSales.get(1));
				long total = price + sale.get(codeSales.get(0));

				long over = String.valueOf(total).length();
				if (over > 10) {
			        System.out.println("合計金額が10桁を超えました");
			        return;
				}

				sale.put(codeSales.get(0), total);

				// System.out.println(sale);

		    } catch(IOException e) {
		    	System.out.println("予期せぬエラーが発生しました");
		    	return;
		    } finally {
		    	if(br !=null) {
		    		try {
		    			br.close();
		    			} catch(IOException e) {
		    				System.out.println("予期せぬエラーが発生しました");
		    				return;
		    			}
		    	}
		    }
		}

		// store:001,○○支店 sale:001,金額

		PrintWriter pw = null;

		try {
			File file = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			for (Map.Entry <String, String> entry : store.entrySet()){
				// System.out.println(entry.getKey() + "," + entry.getValue() + "," + sale.get(entry.getKey()));

			    String all = (entry.getKey() + "," + entry.getValue() + "," + sale.get(entry.getKey()));

			    pw.println(all);
		    }
		} catch(IOException e) {
	    	System.out.println("予期せぬエラーが発生しました");
	    	return;
	    } finally {
	    	if(pw != null) {
	    			pw.close();

	        }
	    }
	}
}
